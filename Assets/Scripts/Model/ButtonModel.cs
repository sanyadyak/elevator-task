namespace Model
{
	public class ButtonModel
	{
		public Direction Direction { get; } = Direction.None;
		public ButtonState State { get; set; } = ButtonState.Enabled;

		public ButtonModel()
		{
		}
		
		public ButtonModel(Direction direction)
		{
			Direction = direction;
		}
	}
}