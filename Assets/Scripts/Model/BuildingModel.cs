namespace Model
{
	public class BuildingModel
	{
		public ElevatorModel Elevator { get; }

		public FloorButtonsModel[] Floors { get; }

		public BuildingModel(BuildingSetupModel buildingSetupModel)
		{
			Elevator = new ElevatorModel(buildingSetupModel.FirstFloor,
			                             buildingSetupModel.FloorCount,
										 new ElevatorLogicModel(new ElevatorRequestInsertLogicModel()));

			int floorsCount = buildingSetupModel.FloorCount;

			Floors = new FloorButtonsModel[floorsCount];

			Floors[0] = new FloorButtonsModel(1, new ButtonModel(Direction.Up));

			for (int i = 0; i < floorsCount; i++)
			{
				ButtonModel[] buttons = GetButtonsForFloor(i, floorsCount);

				Floors[i] = new FloorButtonsModel(i + buildingSetupModel.FirstFloor, buttons);
			}
		}

		private ButtonModel[] GetButtonsForFloor(int floor, int floorsCount)
		{
			if (floor == 0)
			{
				return new[] {new ButtonModel(Direction.Up)};
			}

			if (floor == floorsCount - 1)
			{
				return new[] {new ButtonModel(Direction.Down)};
			}

			return new[] {new ButtonModel(Direction.Up), new ButtonModel(Direction.Down)};
		}

		public void Update(float deltaTime)
		{
			Elevator.Update(deltaTime);
		}
	}
}