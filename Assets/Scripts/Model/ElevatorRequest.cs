namespace Model
{
	public class ElevatorRequest
	{
		public static readonly ElevatorRequest Empty = new ElevatorRequest(-1, Direction.None);
		
		public int Floor { get; }
		public Direction Direction { get; }

		public ElevatorRequest(int floor, Direction direction = Direction.None)
		{
			Floor = floor;
			Direction = direction;
		}
	}
}