namespace Model
{
	public class BuildingSetupModel
	{
		private const int FIRST_FLOOR = 1;
		
		public int FloorCount { get; }
		public int FirstFloor => FIRST_FLOOR;
		
		public BuildingSetupModel(int floorCount)
		{
			FloorCount = floorCount;
		}
	}
}