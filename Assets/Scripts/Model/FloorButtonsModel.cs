using System;

namespace Model
{
	public class FloorButtonsModel
	{
		public ButtonModel[] Buttons { get; }
		
		public int Index { get; }
		
		public event Action Updated = delegate { };
		
		public FloorButtonsModel(int index, ButtonModel button)
		{
			Index = index;
			Buttons = new []{button};
		}
		
		public FloorButtonsModel(int index, ButtonModel[] buttons)
		{
			Index = index;
			Buttons = buttons;
		}

		public void OnPressedButton(ButtonModel button)
		{
			button.State = ButtonState.Pressed;
			
			Updated.Invoke();
		}

		public void OnStoppedAtFloor(Direction direction)
		{
			foreach (var button in Buttons)
			{
				if (button.Direction == direction ||
					button.Direction == Direction.None ||
					direction == Direction.None)
				{
					button.State = ButtonState.Enabled;
				}
			}
			
			Updated.Invoke();
		}
		
		public void UnpressButtons()
		{
			foreach (var button in Buttons)
			{
				if (button.State == ButtonState.Pressed)
				{
					button.State = ButtonState.Enabled;
				}
			}
			
			Updated.Invoke();
		}
	}
}