using System;

namespace Model
{
	public interface IElevatorPositionsModel
	{
		event Action PositionChanged;
		float Position { get; }
	}
}