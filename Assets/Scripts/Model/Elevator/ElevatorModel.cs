using System;
using UnityEngine;

namespace Model
{
	public class ElevatorModel : IElevatorPositionsModel, IElevatorStateModel
	{
		private const float POSITION_PRECISSION = 0.01f;
		private ElevatorLogicModel _logic;

		private float _velocity, _loadUnloadTimeLeft, _loadUnloadTimeTotal;
		private int _destFloor;

		public FloorButtonsModel[] FloorButtons { get; }

		public float Position { get; private set; }

		public ElevatorState State { get; private set; } = ElevatorState.Waiting;

		public Direction Direction { get; private set; } = Direction.None;

		public int DestFloor => _logic.NextRequest.Floor;

		public event Action<int, Direction> StoppedAtFloor = delegate { };
		public event Action PositionChanged = delegate { };
		public event Action StateChanged = delegate { };
		public event Action PressedStop = delegate { };
		
		public ElevatorModel(int firstFloor, int floorsCount, ElevatorLogicModel logic)
		{
			_logic = logic;
			
			Position = firstFloor;
			FloorButtons = new FloorButtonsModel[floorsCount];

			for (int i = 0; i < floorsCount; i++)
			{
				FloorButtons[i] = new FloorButtonsModel(i + firstFloor, new ButtonModel());
			}
		}

		public void SetTimings(float loadUnloadTime, float velocity)
		{
			_velocity = velocity;
			_loadUnloadTimeTotal = loadUnloadTime;
		}

		public void OnPressedFloorButton(ElevatorRequest elevatorRequest)
		{
			_logic.AddRequest(elevatorRequest, Direction, Position);

			if (State == ElevatorState.Moving)
			{
				UpdateMovementDir();
			}
		}
		
		public void OnPressedStop()
		{
			if (_logic.NextRequest == ElevatorRequest.Empty && State == ElevatorState.Waiting)
			{
				return;
			}
			
			_logic.ClearAllRequests();

			foreach (var floorButton in FloorButtons)
			{
				floorButton.UnpressButtons();
			}
			
			State = ElevatorState.Stopped;
			Direction = Direction.None;
			
			PressedStop.Invoke();
			StateChanged.Invoke();
		}

		public void Update(float deltaTime)
		{
			switch (State)
			{
				case ElevatorState.LoadingUndloading:
					ProcessLoadUnload(deltaTime);
					break;
				
				case ElevatorState.Waiting:
					ProcessWaiting();
					break;
				
				case ElevatorState.Stopped:
					ProcessStopped();
					break;

				case ElevatorState.Moving:
					ProcessMovement(deltaTime);
					break;
			}
		}

		private void ProcessLoadUnload(float deltaTime)
		{
			_loadUnloadTimeLeft -= deltaTime;
			if (_loadUnloadTimeLeft > 0) return;

			State = ElevatorState.Waiting;
			StateChanged.Invoke();
		}

		private void ProcessStopped()
		{
			var nextRequest = _logic.NextRequest;
			if (nextRequest == ElevatorRequest.Empty) return;
			
			State = ElevatorState.Moving;
			UpdateMovementDir();
			
			StateChanged.Invoke();
		}

		private void ProcessWaiting()
		{
			var nextRequest = _logic.NextRequest;
			if (nextRequest == ElevatorRequest.Empty) return;

			int roundedPos = Mathf.RoundToInt(Position);
			
			if (Mathf.Abs(roundedPos - nextRequest.Floor) < POSITION_PRECISSION)
			{
				_loadUnloadTimeLeft = _loadUnloadTimeTotal;
				State = ElevatorState.LoadingUndloading;
				
				StoppedAtFloor.Invoke(roundedPos, nextRequest.Direction);
				_logic.OnStoppedAtFloor(roundedPos);
				UpdateMovementDir();
			}
			else
			{
				State = ElevatorState.Moving;
				UpdateMovementDir();
			}
			
			StateChanged.Invoke();
		}

		private void UpdateMovementDir()
		{
			var nextRequest = _logic.NextRequest;
			if (nextRequest == ElevatorRequest.Empty)
			{
				Direction = Direction.None;
				return;
			}

			float delta = nextRequest.Floor - Position;
			
			Direction = delta > 0 ? Direction.Up : Direction.Down;
		}

		private void ProcessMovement(float deltaTime)
		{
			var nextRequest = _logic.NextRequest;
			if (nextRequest == ElevatorRequest.Empty) return;

			int nextRequestedFloor = nextRequest.Floor;


			Position += deltaTime * _velocity * (Direction == Direction.Up ? 1 : -1);

			if ((Direction == Direction.Up && Position >= nextRequestedFloor) ||
				(Direction == Direction.Down && Position <= nextRequestedFloor))
			{
				Position = nextRequestedFloor;
				State = ElevatorState.Waiting;
				StateChanged.Invoke();
			}
			
			PositionChanged.Invoke();
		}
	}
}