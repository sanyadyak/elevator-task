using System.Collections.Generic;

namespace Model
{
	public class ElevatorLogicModel
	{
		private ElevatorRequestInsertLogicModel _insertLogic;
		private List<ElevatorRequest> _queue = new List<ElevatorRequest>();

		public ElevatorRequest NextRequest => _queue.Count > 0 ? _queue[0] : ElevatorRequest.Empty;

		public ElevatorLogicModel(ElevatorRequestInsertLogicModel insertLogic)
		{
			_insertLogic = insertLogic;
		}
		
		public int AddRequest(ElevatorRequest request,
							  Direction currentDirection = Direction.None,
							  float currentPosition = 1)
		{
			if (request == null ||
			    request == ElevatorRequest.Empty)
			{
				return -1;
			}
			
			if (_queue.Count == 0)
			{
				_queue.Add(request);
				return 0;
			}

			int insertIndex = -1;

			for (int i = 0; i < _queue.Count; i++)
			{
				var existingRequest = _queue[i];
				
				if (existingRequest.Floor == request.Floor)
				{
					if (request.Direction == Direction.None || request.Direction == existingRequest.Direction)
					{
						break;
					}
					continue;
				}

				if (i > 0)
				{
					currentDirection = _insertLogic.GetDirBetweenRequests(existingRequest, _queue[i - 1]);
				}
				
				bool canInsert = _insertLogic.CanInsert(request, existingRequest, currentDirection, currentPosition);
				
				if (canInsert)
				{
					insertIndex = i;
					break;
				}

				if (i == _queue.Count - 1)
				{
					insertIndex = _queue.Count;
				}
			}
			
			if (insertIndex != -1) _queue.Insert(insertIndex, request);
			return insertIndex;
		}

		public void OnStoppedAtFloor(int floor)
		{
			if (_queue.Count > 0 && _queue[0].Floor == floor)
			{
				_queue.RemoveAt(0);
			}
		}

		public void ClearAllRequests()
		{
			_queue.Clear();
		}
	}
}