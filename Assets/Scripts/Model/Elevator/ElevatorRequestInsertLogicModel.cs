namespace Model
{
	public class ElevatorRequestInsertLogicModel
	{
		public bool CanInsert(ElevatorRequest newRequest,
							  ElevatorRequest existingRequest,
							  Direction elevatorDirection,
							  float elevatorPosition)
		{
			
			if (newRequest == null ||
				newRequest == ElevatorRequest.Empty ||
				existingRequest == null ||
				existingRequest == ElevatorRequest.Empty)
			{
				return false;
			}

			float deltaFromElevator = newRequest.Floor - elevatorPosition;

			Direction dirFromElevator = deltaFromElevator > 0 ? Direction.Up : Direction.Down;
			
			if (dirFromElevator != elevatorDirection)
			{
				return false;
			}
			
			Direction dirToExisting = GetDirBetweenRequests(existingRequest, newRequest);

			if (elevatorDirection == dirToExisting)
			{
				if (newRequest.Direction == Direction.None || newRequest.Direction == elevatorDirection)
				{
					return true;
				}
			}

			return false;
		}

		public Direction GetDirBetweenRequests(ElevatorRequest first, ElevatorRequest second)
		{
			if (first == null ||
				first == ElevatorRequest.Empty ||
				second == null ||
				second == ElevatorRequest.Empty) return Direction.None;

			int delta = first.Floor - second.Floor;
			
			return delta == 0 ? Direction.None : (delta > 0 ? Direction.Up : Direction.Down);
		}
	}
}