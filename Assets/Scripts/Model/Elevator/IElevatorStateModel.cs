using System;

namespace Model
{
	public interface IElevatorStateModel
	{
		event Action StateChanged;
		ElevatorState State { get; }
		Direction Direction { get; }
		int DestFloor { get; }
	}
}