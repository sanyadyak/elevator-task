public enum ElevatorState
{
	Waiting,
	Moving,
	LoadingUndloading,
	Stopped
}