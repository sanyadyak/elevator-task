public static class TextConsts
{
	public const string SELECT_FLOORS = "Select floors count",
						START = "Start",
						DOORS_OPENED = "Doors are opened",
						MOVING_DOWN = "Moving down to ",
						MOVING_UP = "Moving up to ",
						STOPPED = "Stopped";
}