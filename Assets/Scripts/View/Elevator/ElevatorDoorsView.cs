using Model;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
	public class ElevatorDoorsView : MonoBehaviour
	{
		[SerializeField] private Text _info;
		
		private IElevatorStateModel _model;

		public void SetModel(IElevatorStateModel model)
		{
			_model = model;
			_model.StateChanged += Redraw;
			
			Redraw();
		}

		public void Redraw()
		{
			switch (_model.State)
			{
				case ElevatorState.LoadingUndloading:
					_info.text = TextConsts.DOORS_OPENED;
					break;
				
				case ElevatorState.Stopped:
					_info.text = TextConsts.STOPPED;
					break;
				
				case ElevatorState.Moving:
					if (_model.Direction == Direction.None)
					{
						_info.text = "";
						break;
					}

					_info.text = _model.Direction == Direction.Down ? TextConsts.MOVING_DOWN : TextConsts.MOVING_UP;
					_info.text += _model.DestFloor;
					
					break;
				
				default:
					_info.text = "";
					break;
			}
		}
	}
}