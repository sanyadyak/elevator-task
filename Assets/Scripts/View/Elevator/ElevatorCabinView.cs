using UnityEngine;
using Model;

namespace View
{
	public class ElevatorCabinView : MonoBehaviour
	{
		private float _firstFloorPosY, _heightBetweenFloors;
		private IElevatorPositionsModel _model;

		public void SetModel(IElevatorPositionsModel model)
		{
			_model = model;
			_model.PositionChanged += Redraw;
			
			Redraw();
		}

		public void SetFloorPositions(float first, float betweenFloors)
		{
			_firstFloorPosY = first;
			_heightBetweenFloors = betweenFloors;
			Redraw();
		}
		
		public void Redraw()
		{
			var pos = transform.localPosition;
			pos.y = _firstFloorPosY + _heightBetweenFloors * (_model.Position - 1);
			transform.localPosition = pos;
		}
	}
}