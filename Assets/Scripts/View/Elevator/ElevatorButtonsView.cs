using System;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
	public class ElevatorButtonsView : WindowBaseView
	{
		[SerializeField] private Button _stopButton;
		
		public event Action PressedStop = delegate { }; 
		
		public override void Show()
		{
			_stopButton.onClick.RemoveAllListeners();
			_stopButton.onClick.AddListener(OnPressedStop);
			
			base.Show();
		}
		
		private void OnPressedStop()
		{
			PressedStop.Invoke();
		}
	}
}