using System;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
	public class BuildingSetupView : MonoBehaviour
	{
		[SerializeField] private Slider _floorsSlider;
		[SerializeField] private Text _info, _floorsCountText, _buttonText;

		private int _floorsCount;
		
		public event Action<int> FloorsCountSelected = delegate { };
		
		public void Show(int minFloorCount, int maxFloorCount)
		{
			gameObject.SetActive(true);
			
			_info.text = TextConsts.SELECT_FLOORS;
			_buttonText.text = TextConsts.START;

			_floorsSlider.minValue = minFloorCount;
			_floorsSlider.value = _floorsSlider.maxValue = maxFloorCount;
			OnSliderUpdate();
		}

		public void OnStartPressed()
		{
			FloorsCountSelected.Invoke(_floorsCount);
		}

		public void OnSliderUpdate()
		{
			_floorsCount = Mathf.RoundToInt(_floorsSlider.value);
			_floorsCountText.text = _floorsCount.ToString();
		}
	}
}