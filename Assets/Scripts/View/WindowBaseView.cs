using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View
{
	public class WindowBaseView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
	{
		[SerializeField] private GridLayoutGroup _grid;
		[SerializeField] private RectTransform _contentContainer;
		[SerializeField] private Scrollbar _scrollbar;

		public Transform GridTransform => _grid.transform;

		public float GridCellHeight => _grid.cellSize.y;

		private bool _sizeUpdated;
		
		public virtual void Show()
		{
			_sizeUpdated = false;
			gameObject.SetActive(true);
		}
		
		public virtual void Hide()
		{
			gameObject.SetActive(false);
		}

		private void LateUpdate()
		{
			if (_sizeUpdated || _grid.minHeight < 1) return;

			_sizeUpdated = true;

			float height = _grid.minHeight + GridCellHeight;
			
			_contentContainer.sizeDelta = new Vector2(_contentContainer.sizeDelta.x, height);
			_scrollbar.value = 0;
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		public void OnDrag(PointerEventData eventData)
		{
			transform.Translate(eventData.delta);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
		}
	}
}