using UnityEngine;

namespace View
{
	public class BuildingView : WindowBaseView
	{
		[SerializeField] private GameObject _elevatorInfo;

		public override void Show()
		{
			_elevatorInfo.SetActive(true);
			
			base.Show();
		}

		public override void Hide()
		{
			_elevatorInfo.SetActive(false);
			base.Hide();
		}
	}
}