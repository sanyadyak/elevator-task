using System;
using UnityEngine;
using UnityEngine.UI;
using Model;

namespace View
{
	public class ButtonView : MonoBehaviour
	{
		[SerializeField] private Text _text;
		[SerializeField] private Button _button;
		[SerializeField] private Color _pressedColor;
		
		private Image _image;
		
		private ButtonModel _model;

		public event Action<ButtonModel> ButtonPressed = delegate { }; 

		public void SetModel(ButtonModel model)
		{
			_button.onClick.RemoveAllListeners();
			_button.onClick.AddListener(OnButtonPressed);
			
			_model = model;
			Redraw();
		}

		public void Redraw()
		{
			if (_model == null)
			{
				gameObject.SetActive(false);
				return;
			}

			if (_image == null) _image = GetComponent<Image>();
			_image.color = (_model.State == ButtonState.Pressed) ? _pressedColor : Color.white;

			switch (_model.Direction)
			{
				case Direction.Up:
					_text.text = "/\\";
					break;
				
				case Direction.Down:
					_text.text = "\\/";
					break;
			}
		}

		private void OnButtonPressed()
		{
			ButtonPressed.Invoke(_model);
		}
	}
}