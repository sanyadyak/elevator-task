using System;
using UnityEngine;
using UnityEngine.UI;
using Model;

namespace View
{
	public class FloorButtonsView : MonoBehaviour
	{
		[SerializeField] private Text _floorIndex;
		[SerializeField] private ButtonView[] _buttons;
		
		private FloorButtonsModel _buttonsModel;
		
		public event Action<ButtonModel> ButtonPressed = delegate { }; 

		public void SetFloorModel(FloorButtonsModel buttonsModel)
		{
			_buttonsModel = buttonsModel;

			_floorIndex.text = buttonsModel.Index.ToString();
			
			for (int i = 0; i < _buttons.Length && i < buttonsModel.Buttons.Length; i++)
			{
				_buttons[i].SetModel(buttonsModel.Buttons[i]);
				_buttons[i].ButtonPressed += OnButtonPressed;
			}
			
			_buttonsModel.Updated += Redraw;
			Redraw();
		}

		public void OnButtonPressed(ButtonModel button)
		{
			ButtonPressed.Invoke(button);
		}

		private void Redraw()
		{
			_floorIndex.text = _buttonsModel.Index.ToString();
			foreach (var button in _buttons) button.Redraw();
		}
	}
}