using UnityEngine;
using System;
using Model;
using View;

namespace Controllers
{
	public class BuildingSetupController : MonoBehaviour
	{
		private const int MIN_FLOOR_COUNT = 2,
						  MAX_FLOORS_COUNT = 60;
		
		[SerializeField] private BuildingSetupView _view;

		public event Action<BuildingSetupModel> BuildingSetupComplete = delegate { }; 

		public void Show()
		{
			_view.Show(MIN_FLOOR_COUNT, MAX_FLOORS_COUNT);
			_view.FloorsCountSelected += FloorsCountSelected;
		}

		private void FloorsCountSelected(int floorsCount)
		{
			var setup = new BuildingSetupModel(floorsCount);
			BuildingSetupComplete.Invoke(setup);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
			_view.gameObject.SetActive(false);
		}
	}
}