using UnityEngine;
using Model;
using View;

namespace Controllers
{
	public class ElevatorController : MonoBehaviour
	{
		[SerializeField] private FloorButtonsController _floorButtonsPrefab;
		[SerializeField] private ElevatorButtonsView _buttonsView;

		[SerializeField] private float _loadUnloadTime = 1, 
									   _velocity = 2;
		
		private FloorButtonsController[] _floorButtonsControllers;
		private ElevatorModel _model;
		private bool _viewHeightUpdate;
		
		public void Init(ElevatorModel model)
		{
			_model = model;
			_model.SetTimings(_loadUnloadTime, _velocity);
			_model.StoppedAtFloor += OnStoppedAtFloor;

			var floors = _model.FloorButtons;
			
			InstantiateFloorButtons(floors);

			for (int i = 0; i < floors.Length; i++)
			{
				_floorButtonsControllers[i].Init(floors[i]);
				_floorButtonsControllers[i].PressedFloor += model.OnPressedFloorButton;
			}
		}

		private void InstantiateFloorButtons(FloorButtonsModel[] models)
		{
			_floorButtonsControllers = new FloorButtonsController[models.Length];

			for (int floorIndex = models.Length - 1; floorIndex >= 0; floorIndex--)
			{
				var floorModel = models[floorIndex];
				var floorController = Instantiate(_floorButtonsPrefab, _buttonsView.GridTransform);
				_floorButtonsControllers[floorIndex] = floorController;
			}
		}

		private void OnStoppedAtFloor(int floor, Direction direction)
		{
			foreach (var floorButtonsController in _floorButtonsControllers)
			{
				floorButtonsController.OnElevatorStoppedAtFloor(floor, direction);
			}
		}

		private void OnPressedStop()
		{
			_model.OnPressedStop();
		}
		
		public void Show()
		{
			gameObject.SetActive(true);
			
			_buttonsView.Show();
			_buttonsView.PressedStop += OnPressedStop;
		}
		
		public void Hide()
		{
			gameObject.SetActive(false);
			_buttonsView.gameObject.SetActive(false);
			_floorButtonsPrefab.gameObject.SetActive(false);
			_buttonsView.PressedStop -= OnPressedStop;
		}

		private void OnValidate()
		{
			_model?.SetTimings(_loadUnloadTime, _velocity);
		}
	}
}