using UnityEngine;
using Model;

namespace Controllers
{
	public class RootController : MonoBehaviour
	{
		[SerializeField] private BuildingSetupController buildingSetupController;
		[SerializeField] private BuildingController _buildingController;
		[SerializeField] private ElevatorController _elevatorController;

		private void Awake()
		{
			_buildingController.Hide();
			_elevatorController.Hide();
		}

		void Start()
		{
			buildingSetupController.Show();
			buildingSetupController.BuildingSetupComplete += OnBuildingBuildingSetupComplete;
		}

		private void OnBuildingBuildingSetupComplete(BuildingSetupModel buildingSetupModel)
		{
			buildingSetupController.Hide();
			
			var buildingModel = new BuildingModel(buildingSetupModel);
			
			_buildingController.Init(buildingModel);
			_buildingController.Show();
			
			_elevatorController.Init(buildingModel.Elevator);
			_elevatorController.Show();
		}
	}
}