using UnityEngine;
using UnityEngine.UI;
using Model;
using View;

namespace Controllers
{
	public class BuildingController : MonoBehaviour
	{
		[SerializeField] private FloorButtonsController _floorButtonsPrefab;
		[SerializeField] private BuildingView _view;
		[SerializeField] private ElevatorCabinView _cabinView;
		[SerializeField] private ElevatorDoorsView _doorsView;

		private FloorButtonsController[] _floorButtonsControllers;
		
		private BuildingModel _buildingModel;

		public void Init(BuildingModel buildingModel)
		{
			_buildingModel = buildingModel;
			
			_cabinView.SetModel(_buildingModel.Elevator);
			_doorsView.SetModel(_buildingModel.Elevator);
			
			_buildingModel.Elevator.StoppedAtFloor += OnElevatorStoppedAtFloor;
			_buildingModel.Elevator.PressedStop += OnPressedStop;

			var floors = _buildingModel.Floors;
			
			InstantiateFloorButtons(floors);
			
			for (int i = 0; i < floors.Length; i++)
			{
				_floorButtonsControllers[i].Init(floors[i]);
				_floorButtonsControllers[i].PressedFloor += _buildingModel.Elevator.OnPressedFloorButton;
			}
			
			float height = _view.GridCellHeight * (floors.Length - 1);
			_cabinView.SetFloorPositions(-height / 2, _view.GridCellHeight);
		}

		private void InstantiateFloorButtons(FloorButtonsModel[] models)
		{
			_floorButtonsControllers = new FloorButtonsController[models.Length];

			for (int floorIndex = models.Length - 1; floorIndex >= 0; floorIndex--)
			{
				var floorModel = models[floorIndex];
				var floorController = Instantiate(_floorButtonsPrefab, _view.GridTransform);
				_floorButtonsControllers[floorIndex] = floorController;
			}
		}

		private void OnElevatorStoppedAtFloor(int floor, Direction direction)
		{
			foreach (var floorButtonsController in _floorButtonsControllers)
			{
				floorButtonsController.OnElevatorStoppedAtFloor(floor, direction);
			}
		}

		private void OnPressedStop()
		{
			foreach (var floorButtonsController in _floorButtonsControllers)
			{
				floorButtonsController.OnPressedStop();
			}
		}

		public void Show()
		{
			gameObject.SetActive(true);
			_view.Show();
		}

		public void Hide()
		{
			gameObject.SetActive(false);
			_view.Hide();
			_floorButtonsPrefab.gameObject.SetActive(false);
		}
		

		void Update()
		{
			_buildingModel.Update(Time.deltaTime);
		}
	}
}