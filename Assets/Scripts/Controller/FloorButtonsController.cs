using System;
using UnityEngine;
using Model;
using View;

namespace Controllers
{
	public class FloorButtonsController : MonoBehaviour
	{
		[SerializeField] private FloorButtonsView _view;
		private FloorButtonsModel _model;

		public event Action<ElevatorRequest> PressedFloor = delegate { };

		public void Init(FloorButtonsModel model)
		{
			gameObject.SetActive(true);
			_model = model;
			
			_view.SetFloorModel(_model);
			_view.ButtonPressed += OnPressedButton;
		}

		public void OnPressedButton(ButtonModel buttonModel)
		{
			_model.OnPressedButton(buttonModel);
			PressedFloor.Invoke(new ElevatorRequest(_model.Index, buttonModel.Direction));
		}

		public void OnElevatorStoppedAtFloor(int floor, Direction direction)
		{
			if (_model.Index != floor) return;
			_model.OnStoppedAtFloor(direction);
		}

		public void OnPressedStop()
		{
			_model.UnpressButtons();
		}
	}
}