﻿using NUnit.Framework;
using Model;

public class ElevatorLogicModelTest
{
    [Test]
    public void Should_Skip_SpawnSameRequest()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());
        logic.AddRequest(new ElevatorRequest(1, Direction.Up));

        Assert.Negative(logic.AddRequest(new ElevatorRequest(1, Direction.Up)));
    }

    [Test]
    public void Should_Skip_SpawnNullRequest()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        Assert.Negative(logic.AddRequest(null));
    }

    [Test]
    public void Should_Skip_SpawnEmptyRequest()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        Assert.Negative(logic.AddRequest(null));
    }
    
    [Test]
    public void Should_InsertFirst_SpawnRequestBeforeExistingInSameDirection()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        logic.AddRequest(new ElevatorRequest(10));

        Assert.True(0 == logic.AddRequest(new ElevatorRequest(5, Direction.Up), Direction.Up));
    }
    
    [Test]
    public void Should_AddToEnd_SpawnRequestBeforeExistingInOppositeDirection()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        logic.AddRequest(new ElevatorRequest(10));

        Assert.True(1 == logic.AddRequest(new ElevatorRequest(5, Direction.Down), Direction.Up));
    }
    
    [Test]
    public void Should_AddToEnd_SpawnRequestBeforeExistingInSameDirectionAfterElevator()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        logic.AddRequest(new ElevatorRequest(10));

        Assert.True(1 == logic.AddRequest(new ElevatorRequest(2, Direction.Up), Direction.Up, 5));
    }
    
    [Test]
    public void Should_Skip_SpawnRequestFromElevatorWhenExistingWithSameFloor()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        logic.AddRequest(new ElevatorRequest(5, Direction.Up));

        Assert.Negative(logic.AddRequest(new ElevatorRequest(5), Direction.Up));
    }
    
    [Test]
    public void Should_Insert_SpawnRequestBetweenExistingInNoneDirection()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        logic.AddRequest(new ElevatorRequest(5));
        logic.AddRequest(new ElevatorRequest(10));

        Assert.True(1 == logic.AddRequest(new ElevatorRequest(6), Direction.Up));
    }
    
    [Test]
    public void Should_Insert_SpawnRequestBetweenExistingInSameDirection()
    {
        var logic = new ElevatorLogicModel(new ElevatorRequestInsertLogicModel());

        logic.AddRequest(new ElevatorRequest(5));
        logic.AddRequest(new ElevatorRequest(10), Direction.Up);

        Assert.True(1 == logic.AddRequest(new ElevatorRequest(6, Direction.Up), Direction.Up));
    }
}
